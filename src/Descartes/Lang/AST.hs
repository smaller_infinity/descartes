module Descartes.Lang.AST() where

-- |Core language definition.
data DescartesAST a = ForAll [Int] (DescartesAST a)
                    | Exists [Int] (DescartesAST a)
                    | Conjugation [DescartesAST a]
                    | Disjunction [DescartesAST a]
                    | Negation (DescartesAST a)
                    | Term (Term a)

-- |A term is either a variable or some existing value from the knowledge base.
data Term a = Var Int
            | Value a
